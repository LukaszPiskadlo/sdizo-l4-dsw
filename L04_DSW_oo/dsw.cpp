#include <iostream>
#include <ctime>

using namespace std;

class BST
{
public:
	BST();
	BST(int value);
	~BST();

	void add(int value);
	int find(int value);
	void printPreorder();
	void printInorder();
	void printPostorder();
	void remove(int value);

protected:
	class Node
	{
	public:
		Node(int value);

		int getValue();
		Node* getLeft();
		Node* getRight();

		void setValue(int value);
		void setLeft(Node* left);
		void setRight(Node* right);

	private:
		int value;
		Node* left;
		Node* right;
	};

	Node* root;

	void add(int value, Node* node);
	Node* find(int value, Node* node);
	void preorder(Node* node);
	void inorder(Node* node);
	void postorder(Node* node);
	Node* remove(int value, Node* node);
	Node* findSuccessor(Node* node);
	Node* removeSuccessor(Node* node);
	void free(Node* node);
};

class DSWTree : public BST
{
public:
	DSWTree();
	DSWTree(int value);

	void dsw();
	int getRootValue();

private:
	void rotateLeft(Node* parent, Node* node);
	void rotateRight(Node* parent, Node* node);
	void makeList();
	void rotate(int limit);
	void balanceTree();
};

BST::Node::Node(int value) : value(value)
{
	left = nullptr;
	right = nullptr;
}

int BST::Node::getValue()
{
	return value;
}

BST::Node* BST::Node::getLeft()
{
	return left;
}

BST::Node* BST::Node::getRight()
{
	return right;
}

void BST::Node::setValue(int value)
{
	this->value = value;
}

void BST::Node::setLeft(Node* left)
{
	this->left = left;
}

void BST::Node::setRight(Node* right)
{
	this->right = right;
}

BST::BST()
{
	root = nullptr;
}

BST::BST(int value)
{
	root = new Node(value);
}

BST::~BST()
{
	free(root);
}

void BST::add(int value)
{
	add(value, root);
}

int BST::find(int value)
{
	Node* node = find(value, root);
	return (node == nullptr) ? NULL : node->getValue();
}

void BST::printPreorder()
{
	cout << "Preorder:" << endl;
	preorder(root);
	cout << endl;
}

void BST::printInorder()
{
	cout << "Inorder:" << endl;
	inorder(root);
	cout << endl;
}

void BST::printPostorder()
{
	cout << "Postorder:" << endl;
	postorder(root);
	cout << endl;
}

void BST::remove(int value)
{
	root = remove(value, root);
}

void BST::add(int value, Node* node)
{
	if (root == nullptr)
		root = new Node(value);
	else
	{
		if (node->getValue() == value)
			cout << "Element o wartosci " << value << " juz istnieje!\n";
		else if (node->getValue() > value)
		{
			if (node->getLeft() == nullptr)
				node->setLeft(new Node(value));
			else
				add(value, node->getLeft());
		}
		else
		{
			if (node->getRight() == nullptr)
				node->setRight(new Node(value));
			else
				add(value, node->getRight());
		}
	}
}

BST::Node* BST::find(int value, Node* node)
{
	if (node == nullptr)
		cout << "Element o wartosci " << value << " nie istnieje!\n";
	else
	{
		if (node->getValue() == value)
			cout << "Znaleziono: " << node->getValue() << endl;
		else if (node->getValue() > value)
			node = find(value, node->getLeft());
		else
			node = find(value, node->getRight());
	}

	return node;
}

void BST::preorder(Node* node)
{
	if (node != nullptr)
	{
		cout << node->getValue() << ", ";
		preorder(node->getLeft());
		preorder(node->getRight());
	}
}

void BST::inorder(Node* node)
{
	if (node != nullptr)
	{
		inorder(node->getLeft());
		cout << node->getValue() << ", ";
		inorder(node->getRight());
	}
}

void BST::postorder(Node* node)
{
	if (node != nullptr)
	{
		postorder(node->getLeft());
		postorder(node->getRight());
		cout << node->getValue() << ", ";
	}
}

BST::Node* BST::remove(int value, Node* node)
{
	if (node == nullptr)
	{
		cout << "Element o wartosci " << value << " nie istnieje!\n";
		return node;
	}
	else if (node->getValue() > value)
		node->setLeft(remove(value, node->getLeft()));
	else if (node->getValue() < value)
		node->setRight(remove(value, node->getRight()));
	else
	{
		if (node->getLeft() == nullptr && node->getRight() == nullptr)
		{
			delete node;
			node = nullptr;
		}
		else if (node->getLeft() == nullptr)
		{
			Node* toRemove = node;
			node = node->getRight();
			delete toRemove;
		}
		else if (node->getRight() == nullptr)
		{
			Node* toRemove = node;
			node = node->getLeft();
			delete toRemove;
		}
		else
		{
			Node* toRemove = node;
			node = findSuccessor(toRemove->getRight());
			node->setRight(removeSuccessor(toRemove->getRight()));
			node->setLeft(toRemove->getLeft());
			delete toRemove;
		}
		cout << "Usunieto " << value << endl;
	}

	return node;
}

BST::Node* BST::findSuccessor(Node* node)
{
	if (node->getLeft() == nullptr)
		return node;

	return findSuccessor(node->getLeft());
}

BST::Node* BST::removeSuccessor(Node* node)
{
	if (node->getLeft() == nullptr)
		return node->getRight();

	node->setLeft(removeSuccessor(node->getLeft()));

	return node;
}

void BST::free(Node* node)
{
	if (node != nullptr)
	{
		free(node->getLeft());
		free(node->getRight());
		delete node;
	}
}

DSWTree::DSWTree() : BST()
{
}

DSWTree::DSWTree(int value) : BST(value)
{
}

void DSWTree::dsw()
{
	makeList();
	cout << "Pierwsza faza DSW:\n";
	printPreorder();
	balanceTree();
	cout << "Po zbalansowaniu:\n";
	printPreorder();
}

int DSWTree::getRootValue()
{
	return root->getValue();
}

void DSWTree::rotateLeft(Node* parent, Node* node)
{
	Node* child = node->getRight();

	if (parent != nullptr)
		if (parent->getRight() == node)
			parent->setRight(child);
		else
			parent->setLeft(child);
	else
		root = child;

	node->setRight(child->getLeft());
	child->setLeft(node);
}

void DSWTree::rotateRight(Node* parent, Node* node)
{
	Node* child = node->getLeft();

	if (parent != nullptr)
		if (parent->getRight() == node)
			parent->setRight(child);
		else
			parent->setLeft(child);
	else
		root = child;

	node->setLeft(child->getRight());
	child->setRight(node);
}

void DSWTree::makeList()
{
	Node* node = root;
	Node* parent = nullptr;
	Node* child = nullptr;

	while (node != nullptr)
	{
		if (node->getLeft() != nullptr)
		{
			child = node->getLeft();
			rotateRight(parent, node);
			node = child;
		}
		else
		{
			parent = node;
			node = node->getRight();
		}
	}
}

void DSWTree::rotate(int limit)
{
	Node* node = root;
	Node* parent = nullptr;
	Node* child = nullptr;

	for (int i = 0; i < limit; i++)
	{
		child = node->getRight();
		if (child != nullptr)
		{
			rotateLeft(parent, node);
			parent = child;
			node = child->getRight();
		}
	}
}

void DSWTree::balanceTree()
{
	Node* node = root;

	int nodesNumber = 0;
	do {
		nodesNumber++;
		node = node->getRight();
	} while (node != nullptr);

	int m = 1;
	while (m <= nodesNumber)
		m = m * 2 + 1;

	m /= 2;

	rotate(nodesNumber - m);

	while (m > 1)
	{
		m /= 2;
		rotate(m);
	}
}

int main()
{
	DSWTree* dsw = new DSWTree(100);
	srand((unsigned int)time(NULL));
	for (int i = 0; i < 10; i++)
		dsw->add(rand() % 300);

	cout << "Po utworzeniu drzewa:\n";
	dsw->printPreorder();

	dsw->dsw();

	dsw->add(rand() % 300);

	dsw->remove(dsw->getRootValue());
	cout << "Po usunieciu roota:\n";
	dsw->printPreorder();

	delete dsw;

	system("pause");
	return 0;
}
